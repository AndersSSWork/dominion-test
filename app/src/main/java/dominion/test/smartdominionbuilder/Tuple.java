package dominion.test.smartdominionbuilder;

import java.util.Comparator;

/**
 * Created by Anders on 02-04-2018.
 */

public class Tuple<T1, T2> implements Comparable<Tuple<T1, T2>> {

    public T1 first;
    public T2 second;
    public TupleSort<T1, T2> sorter;

    public Tuple(T1 firstAssign, T2 secondAssign){
        first = firstAssign;
        second = secondAssign;
    }

    public Tuple(){

    }

    public int compareTo(Tuple<T1, T2> a){
        if(sorter == null){
            return 0;
        }
        float sortedValue = sorter.differenceTuple(this, a);

       if(sortedValue > 0){
           return 1;
       }
       else if(sortedValue < 0){
           return -1;
       }
       else {
           return 0;
       }
    }
}
