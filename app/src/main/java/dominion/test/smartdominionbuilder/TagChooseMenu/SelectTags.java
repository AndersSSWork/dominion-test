package dominion.test.smartdominionbuilder.TagChooseMenu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import dominion.test.smartdominionbuilder.CardChooseMenu.SelectCardActivity;
import dominion.test.smartdominionbuilder.EnterNewCard.ListTagRadio;
import dominion.test.smartdominionbuilder.R;
import dominion.test.smartdominionbuilder.Tuple;

public class SelectTags extends AppCompatActivity {

    private String[] tags;
    private tagMenuItem[] tagItems;
    private String[] cards;

    public ArrayList<String> tagList;
    public ArrayList<Tuple<String, ListTagTwoChoice.Choice>> chosenTagsList;

    public ListTagTwoChoice adapter;
    public ListView tagsToChooseView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_tags);

        CreateMenu();
    }

    public void CreateMenu(){
        Intent intent = getIntent();
        tags = intent.getStringArrayExtra("tags");
        cards = intent.getStringArrayExtra("cards");
        tagList = new ArrayList<>();

        chosenTagsList = new ArrayList<>();
        if(tags != null) {
            for (String toAdd : tags) {
                tagList.add(toAdd);
            }
        }
        tagsToChooseView = findViewById(R.id.tagListChoose);
        adapter = new ListTagTwoChoice(tagList, chosenTagsList, this);
        tagsToChooseView.setAdapter(adapter);

        adapter.notifyDataSetChanged();
    }

    public void FinishSelecting(View view){
        List<String> chosenTags = new ArrayList<String>();
        List<String> bannedTags = new ArrayList<>();
        for(int i = 0; i < chosenTagsList.size(); ++i){
            if(chosenTagsList.get(i).second == ListTagTwoChoice.Choice.ban){
                bannedTags.add(chosenTagsList.get(i).first);
            }
            else {
                chosenTags.add(chosenTagsList.get(i).first);
            }
        }

        Intent cardAct = new Intent(this, SelectCardActivity.class);
        cardAct.putExtra("tags", tags);
        cardAct.putExtra("cards", cards);
        cardAct.putExtra("banTag", bannedTags.toArray());
        cardAct.putExtra("mustTag", chosenTags.toArray());
        cardAct.putExtra("settings", getIntent().getStringExtra("settings"));
        cardAct.putExtra("specSetting", getIntent().getIntExtra("specSetting", 3));
        startActivity(cardAct);
    }
}
