package dominion.test.smartdominionbuilder.TagChooseMenu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import dominion.test.smartdominionbuilder.R;
import dominion.test.smartdominionbuilder.Tuple;

public class ListTagTwoChoice extends BaseAdapter implements ListAdapter {
    private ArrayList<String> list = new ArrayList<String>();
    private Context context;
    public enum Choice{choose, ban};
    private ArrayList<Tuple<String, Choice>> checkList = new ArrayList<Tuple<String, Choice>>();


    public ListTagTwoChoice(ArrayList<String> usedTags, ArrayList<Tuple<String, Choice>> output, Context context) {
        this.list = usedTags;
        this.context = context;
        this.checkList = output;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        //return list.get(pos).getId();
        //just return 0 if your list items do not have an Id variable.
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_tag_two_choice, null);
        }

        //Handle TextView and display string from your list
        TextView listItemText = (TextView)view.findViewById(R.id.TagText);
        listItemText.setText(list.get(position));

        //Handle buttons and add onClickListeners
        CheckBox checkChoose = (CheckBox) view.findViewById(R.id.cardChoose);

        checkChoose.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //do something
                boolean needAdd = true;
                for(int i = 0; i < checkList.size(); ++i){
                    if(checkList.get(i).first.equals(list.get(position))){
                        needAdd = false;
                        if(checkList.get(i).second.equals(Choice.choose)){
                            checkList.remove(i);
                        }
                        else {
                            checkList.get(i).second = Choice.choose;
                        }
                        break;
                    }
                }
                if(needAdd){
                    checkList.add(new Tuple<String, Choice>(list.get(position), Choice.choose));
                }
            }
        });

        CheckBox checkBan = (CheckBox) view.findViewById(R.id.tagBan);

        checkBan.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //do something
                boolean needAdd = true;
                for(int i = 0; i < checkList.size(); ++i){
                    if(checkList.get(i).first.equals(list.get(position))){
                        needAdd = false;
                        if(checkList.get(i).second.equals(Choice.ban)){
                            checkList.remove(i);
                        }
                        else {
                            checkList.get(i).second = Choice.ban;
                        }
                        break;
                    }
                }
                if(needAdd){
                    checkList.add(new Tuple<String, Choice>(list.get(position), Choice.ban));
                }
            }
        });
        return view;
    }

    @Override
    public String toString(){
        return "it calls this";
    }
}