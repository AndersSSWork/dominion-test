package dominion.test.smartdominionbuilder.EnterNewTag;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import dominion.test.smartdominionbuilder.FileLoader;
import dominion.test.smartdominionbuilder.MainMenu;
import dominion.test.smartdominionbuilder.R;
import dominion.test.smartdominionbuilder.SettingSpecSpinner;

public class CreateTagActivity extends AppCompatActivity {

    private int _openSpecSetting = 0;
    public String[] possibleSpecs;
    FileLoader loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_create_tag);

        loader = new FileLoader(getApplicationContext());
        possibleSpecs = loader.getPosSpecSettings();

        Spinner specSpinner = findViewById(R.id.spinner_create_tag);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_dropdown_item, possibleSpecs);
        specSpinner.setAdapter(spinnerAdapter);
        specSpinner.setOnItemSelectedListener(new SettingSpecSpinner(this));

    }

//TODO this needs a way to choose the spec setting
    public void Accept(View view){
        Intent intent = new Intent(this, MainMenu.class);
        EditText editTag = findViewById(R.id.newTagEdit);

        //intent.putExtra("newTag", toPut); //Deprecated using FileLoader
        loader.changeSpecSettings(loader.getPosSpecSettings()[_openSpecSetting]);
        boolean testSave = loader.saveSingleTag(editTag.getText().toString());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void Cancel(View view){
        Intent intent = new Intent(this, MainMenu.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void changeOpenSpec(int newOpen){
        _openSpecSetting = newOpen;
    }

}
