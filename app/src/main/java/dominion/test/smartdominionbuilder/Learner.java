package dominion.test.smartdominionbuilder;

/**
 * Created by Anders on 08-04-2018.
 */

public class Learner {

    //TODO should probably be a part of the settings
    public float learnRateSingle = 0.1f;
    public float learnRateSyn    = 0.1f;

    public void JudgeAll(Card[] toJudge, boolean wasGood){
        for(Card singleJudge : toJudge){
            for(Card checkTag : toJudge){
                if(!singleJudge.name.equals(checkTag.name)){
                    for(Tuple<String, Float> synSingle : singleJudge.tagSynergy){
                        for(String tagCheck : checkTag.tags){
                            if(tagCheck.equals(synSingle.first)){
                                synSingle.second += wasGood ? -1*learnRateSyn : learnRateSyn;
                            }
                        }
                    }
                }
            }
            singleJudge.indivRate += wasGood ? learnRateSingle : -1*learnRateSingle;
            if(singleJudge.indivRate > Card.maxIndiv){
                singleJudge.indivRate = Card.maxIndiv;
            }
            else if(singleJudge.indivRate < 0){
                singleJudge.indivRate = 0;
            }
        }
    }

    public void JudgeSyn(Card[] toJudge, boolean wasGood){
        for(Card singleJudge : toJudge){
            for(Card checkTag : toJudge){
                if(!singleJudge.name.equals(checkTag.name)){
                    for(Tuple<String, Float> synSingle : singleJudge.tagSynergy){
                        for(String tagCheck : checkTag.tags){
                            if(tagCheck.equals(synSingle.first)){
                                synSingle.second += wasGood ? -1*learnRateSyn : learnRateSyn;
                            }
                        }
                    }
                }
            }
            if(singleJudge.indivRate > Card.maxIndiv){
                singleJudge.indivRate = Card.maxIndiv;
            }
            else if(singleJudge.indivRate < 0){
                singleJudge.indivRate = 0;
            }
        }

    }

    public void JudgeSingle(Card toJudge, boolean wasGood){
        toJudge.indivRate += wasGood ? learnRateSingle : -1*learnRateSingle;
        if(toJudge.indivRate > Card.maxIndiv){
            toJudge.indivRate = Card.maxIndiv;
        }
        else if(toJudge.indivRate < 0){
            toJudge.indivRate = 0;
        }
    }
}
