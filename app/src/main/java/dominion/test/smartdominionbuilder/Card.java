package dominion.test.smartdominionbuilder;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Anders on 02-04-2018.
 */

public class Card {
    public String name; //name of the card
    public String[] tags; //what _tags describe the card
    public Tuple<String, Float>[]tagSynergy; //what _tags the card synergizes with
    public float indivRate; //how much the user rates the individual card, higher is more frequent
    public int timeLastUsed; //how long it's been since it was last used
    public String pictureFilename; //the filename for the card picture - not gonna be used

    public static float maxIndiv = 50f;
    public static int maxTimeLastUsed = 5;

    @Nullable
    public JSONObject toJSON(){
        JSONObject singleObject = new JSONObject();

        try {
            singleObject.put("name", name);
            singleObject.put("amountTags", tags.length);
            for(int i = 0; i < tags.length; ++i){
                singleObject.put("tag"+i, tags[i]);
            }
            for(int i = 0; i < tagSynergy.length; ++i){
                singleObject.put(tagSynergy[i].first, tagSynergy[i].second);
            }
            singleObject.put("indivRate", indivRate);
            singleObject.put("lastUsed", timeLastUsed);
            singleObject.put("pictureFilename", pictureFilename);
        }
        catch (org.json.JSONException e){
            return null;
        }
        //JSONArray returnValue = new JSONArray();
        //returnValue.put(singleObject);
        return singleObject;
    }

    public Card(String toReadString, String[] tags){
        try {
            JSONObject toRead = new JSONObject(toReadString);
            readJSON(toRead, tags);
        }
        catch (JSONException e){
            return;
        }
    }

    public Card(JSONObject toRead, String[] allTags){
        readJSON(toRead, allTags);
    }

    /*
    This is for creating a non-existent card
     */
    public Card(String newName, String[] allTags, String[] containedTags) {
        String test  = "";
        name = newName;
        tags = containedTags;
        tagSynergy = new Tuple[allTags.length];
        for (int i = 0; i < allTags.length; ++i) {
            //Maybe figure out a better default value than 1f
            tagSynergy[i] = new Tuple<String, Float>(allTags[i], 1f);
        }
        indivRate = 1f;
        timeLastUsed = 0;
        pictureFilename = null;
    }

    public void readJSON(JSONObject toRead, String[] allTags){
        try {
            name = toRead.getString("name");
            int amountTags = toRead.getInt("amountTags");
            tags = new String[amountTags];
            for (int i = 0; i < amountTags; ++i) {
                tags[i] = toRead.getString("tag" + i);
            }
            indivRate = (float)toRead.getDouble("indivRate");
            timeLastUsed = toRead.getInt("lastUsed");
            pictureFilename = null;
        }
        catch (JSONException e){
                name = "failure reading card";
                tags = new String[0];
                indivRate = 0f;
                timeLastUsed = 5;
                pictureFilename = null;
        }
        if(allTags != null) {
            tagSynergy = new Tuple[allTags.length];
            for (int i = 0; i < allTags.length; ++i) {
                try {
                    tagSynergy[i] = new Tuple<String, Float>(allTags[i], (float) toRead.getDouble(allTags[i]));
                } catch (JSONException e) {
                    tagSynergy[i] = new Tuple<String, Float>(allTags[i], 1f);
                }
            }
        }
        else {
            tagSynergy = new Tuple[0];
        }
    }

    @Override
    public String toString(){
        return toJSON().toString();
    }
}
