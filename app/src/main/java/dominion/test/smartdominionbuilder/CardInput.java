package dominion.test.smartdominionbuilder;

import android.content.Context;
import android.widget.TextView;

import dominion.test.smartdominionbuilder.LoadScreen.LoadCardsActivity;

/**
 * Created by Anders on 06-04-2018.
 */

public class CardInput {
    public Card[] availableCards;
    public Card[] reqCards;
    public String[] reqTags;
    public int deckSize;
    public int clusterSize;
    public int maxCardUseTime;
    public TextView textToUpdate;
    public LoadCardsActivity finishCall;
    public Context finishContext;
}
