package dominion.test.smartdominionbuilder.CardChooseMenu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import dominion.test.smartdominionbuilder.Card;
import dominion.test.smartdominionbuilder.LoadScreen.LoadCardsActivity;
import dominion.test.smartdominionbuilder.R;
import dominion.test.smartdominionbuilder.Tuple;

public class SelectCardActivity extends AppCompatActivity {

    public Card[] _cards;
    public String[] _cardsString;
    public String[] _tags;
    public listCardSingleChoice _tagAdapter;
    public ListView _theListView;

    public ArrayList<String> allCardsNames;
    public ArrayList<Tuple<String, listCardSingleChoice.Choice>> chosenCards;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_card);
        CreateMenu();
    }

    public void CreateMenu(){
        Intent intent = getIntent();
        _cardsString = intent.getStringArrayExtra("cards");
        _tags = intent.getStringArrayExtra("tags");
        _cards = new Card[_cardsString.length];
        allCardsNames = new ArrayList<>();
        for(int i = 0; i < _cardsString.length; ++i){
            _cards[i] = new Card(_cardsString[i], _tags);
            allCardsNames.add(_cards[i].name);
        }
        chosenCards = new ArrayList<>();
        _tagAdapter = new listCardSingleChoice(allCardsNames, chosenCards, getApplicationContext());
        _theListView = findViewById(R.id.cardListChoose);
        _theListView.setAdapter(_tagAdapter);
        _tagAdapter.notifyDataSetChanged();
    }

    public void FinishSelecting(View view){
        List<String> mustCards = new ArrayList<String>();
        List<String> chosenBan = new ArrayList<>();
        for(int i = 0; i < chosenCards.size(); ++i){
            if(chosenCards.get(i).second == listCardSingleChoice.Choice.ban){
                chosenBan.add(chosenCards.get(i).first);
            }
            else {
                mustCards.add(chosenCards.get(i).first);
            }
        }

        Intent intent = getIntent();
        Intent loadMenu = new Intent(this, LoadCardsActivity.class);
        String[] mustCardsArray = new String[mustCards.size()];
        for(int i = 0; i < mustCardsArray.length; ++i){
            mustCardsArray[i] = mustCards.get(i);
        }
        String[] banCardsArray = new String[chosenBan.size()];
        for(int i = 0; i < banCardsArray.length; ++i){
            banCardsArray[i] = chosenBan.get(i);
        }
        loadMenu.putExtra("cards", _cardsString);
        loadMenu.putExtra("tags", _tags);
        loadMenu.putExtra("banTag", intent.getStringArrayExtra("banTag"));
        loadMenu.putExtra("mustTag", intent.getStringArrayExtra("mustTag"));
        loadMenu.putExtra("banCard", banCardsArray);
        loadMenu.putExtra("mustCard", mustCardsArray);
        loadMenu.putExtra("settings", intent.getStringExtra("settings"));
        loadMenu.putExtra("specSetting", intent.getIntExtra("specSetting", 3));
        startActivity(loadMenu);
    }
}
