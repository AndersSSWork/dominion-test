package dominion.test.smartdominionbuilder.CardChooseMenu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import dominion.test.smartdominionbuilder.R;
import dominion.test.smartdominionbuilder.TagChooseMenu.ListTagTwoChoice;
import dominion.test.smartdominionbuilder.Tuple;

public class listCardSingleChoice extends BaseAdapter implements ListAdapter {
    private ArrayList<String> list = new ArrayList<String>();
    public enum Choice{choose, ban};
    private ArrayList<Tuple<String, listCardSingleChoice.Choice>> selectedCards = new ArrayList<Tuple<String, listCardSingleChoice.Choice>>();
    private Context context;

    public listCardSingleChoice(ArrayList<String> usedCards, ArrayList<Tuple<String, listCardSingleChoice.Choice>> output, Context usedContext) {
        list = usedCards;
        selectedCards = output;
        context = usedContext;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        //return list.get(pos).getId();
        //just return 0 if your list items do not have an Id variable.
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_card_two_choice, null);


        //Handle TextView and display string from your list
        TextView listItemText = (TextView)view.findViewById(R.id.cardText);
        listItemText.setText(list.get(position));

        //Handle buttons and add onClickListeners
        CheckBox checkChoose = (CheckBox) view.findViewById(R.id.cardChoose);

        checkChoose.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //do something
                boolean needAdd = true;
                for(int i = 0; i < selectedCards.size(); ++i){
                    if(selectedCards.get(i).first.equals(list.get(position))){
                        needAdd = false;
                        if(selectedCards.get(i).second.equals(ListTagTwoChoice.Choice.choose)){
                            selectedCards.remove(i);
                        }
                        else {
                            selectedCards.get(i).second = listCardSingleChoice.Choice.choose;
                        }
                        break;
                    }
                }
                if(needAdd){
                    selectedCards.add(new Tuple<String, listCardSingleChoice.Choice>(list.get(position), listCardSingleChoice.Choice.choose));
                }
            }
        });

        CheckBox checkBan = (CheckBox) view.findViewById(R.id.banCard);

        checkBan.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //do something
                boolean needAdd = true;
                for(int i = 0; i < selectedCards.size(); ++i){
                    if(selectedCards.get(i).first.equals(list.get(position))){
                        needAdd = false;
                        if(selectedCards.get(i).second.equals(ListTagTwoChoice.Choice.ban)){
                            selectedCards.remove(i);
                        }
                        else {
                            selectedCards.get(i).second = listCardSingleChoice.Choice.ban;
                        }
                        break;
                    }
                }
                if(needAdd){
                    selectedCards.add(new Tuple<String, listCardSingleChoice.Choice>(list.get(position), listCardSingleChoice.Choice.ban));
                }
            }
        });

        return view;
    }

    @Override
    public String toString(){
        return "it calls this";
    }
}