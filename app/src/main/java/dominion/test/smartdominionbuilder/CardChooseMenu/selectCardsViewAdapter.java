package dominion.test.smartdominionbuilder.CardChooseMenu;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import dominion.test.smartdominionbuilder.Card;
import dominion.test.smartdominionbuilder.R;
import dominion.test.smartdominionbuilder.Tuple;

public class selectCardsViewAdapter extends RecyclerView.Adapter<selectCardsViewAdapter.cardViewHolder> {


    private cardMenuItem[] cards;

    public class cardViewHolder extends RecyclerView.ViewHolder{
        public CheckBox chooseCard;
        public CheckBox banCard;
        public TextView cardText;

        public cardViewHolder(View view){
            super(view);
            chooseCard = (CheckBox) view.findViewById(R.id.cardChoose);
            banCard = (CheckBox) view.findViewById(R.id.cardBan);
            this.setIsRecyclable(false);
        }
    }

    public selectCardsViewAdapter(cardMenuItem[] allCards){
        cards = allCards;
    }

    @Override
    public cardViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_layout, parent, false);
        return new cardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(cardViewHolder holder, int pos){
        if(pos < cards.length){
            holder.cardText.setText(cards[pos].theCard.name);
            cards[pos].isMust = holder.chooseCard;
            cards[pos].isBan = holder.banCard;
        }
    }

    @Override
    public int getItemCount(){
        if(cards == null){
            return 0;
        }
        return cards.length;
    }

    public cardMenuItem getItem(int pos){
        if(cards.length >= pos){
            return null;
        }
        else {
            return cards[pos];
        }
    }

}
