package dominion.test.smartdominionbuilder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


import dominion.test.smartdominionbuilder.EnterNewCard.CreateCardActivity;
import dominion.test.smartdominionbuilder.EnterNewTag.CreateTagActivity;
import dominion.test.smartdominionbuilder.SettingsMenu.CustomSettings;
import dominion.test.smartdominionbuilder.TagChooseMenu.SelectTags;

public class MainMenu extends AppCompatActivity {

    Card[] _allCards;
    String[] _tags;

    private JSONObject _specSettings;
    private FileLoader _fileLoader;
    private Spinner _chosenSetting;
    public  int openSetting = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        Intent intent = getIntent();
        //TODO use the fileloader for this, need to make learning use it first though
        String[] learnedCardsString = intent.getStringArrayExtra("cards");

        _fileLoader = new FileLoader(getApplicationContext());

        //TODO needs to know which spec setting to use
        try {
            _tags = _fileLoader.LoadTags();
            _allCards = _fileLoader.LoadCards();
        }
        catch (JSONException e){
            _tags = null;
            _allCards = null;
        }

        //This is where it ends up even when creating new tags. make the new tags and cards use FileLoader
        if(_tags == null){
            _displayError("no tags have been found, please create some.");
        }
        else if(_allCards == null){
            _displayError("no cards have been found, please create some.");
        }
        else {
            if (learnedCardsString != null && _tags != null && _allCards != null) {
                List<Card> learnedCards = new ArrayList<>();
                for (int i = 0; i < learnedCardsString.length; ++i) {
                    learnedCards.add(new Card(learnedCardsString[i], _tags));
                }
                for (int i = 0; i < _allCards.length; ++i) {
                    for (int j = 0; j < learnedCards.size(); ++j) {
                        if (_allCards[i].name.equals(learnedCards.get(j).name)) {
                            _allCards[i] = learnedCards.get(j);
                            learnedCards.remove(j);
                        }
                    }
                }
            }

            //The introduction  of fileloader deprecated this
    /*        String addedCardsString = intent.getStringExtra("newCard");
            if (addedCardsString != null) {
                if (_allCards != null) {
                    Card newCard = new Card(addedCardsString, _tags);
                    Card[] newAllCards = new Card[_allCards.length + 1];
                    System.arraycopy(_allCards, 0, newAllCards, 0, _allCards.length);
                    newAllCards[_allCards.length] = newCard;
                    _allCards = newAllCards;
                } else {
                    _allCards = new Card[1];
                    _allCards[0] = new Card(addedCardsString, _tags);
                }
            }

            String addedTagString = intent.getStringExtra("newTag");
            if (addedTagString != null) {
                if (_tags != null) {
                    String[] newAllTags = new String[_tags.length + 1];
                    System.arraycopy(_tags, 0, newAllTags, 0, _tags.length);
                    newAllTags[_tags.length] = addedTagString;
                    _tags = newAllTags;
                } else {
                    _tags = new String[1];
                    _tags[0] = addedTagString;
                }
            }
            */
            _fileLoader.saveTags(_tags);
            _fileLoader.saveCards(_allCards);
        }
        String[] possibleSpecs = _fileLoader.getPosSpecSettings();
        _chosenSetting = findViewById(R.id.spinner_settings_main);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_dropdown_item, possibleSpecs);
        _chosenSetting.setAdapter(spinnerAdapter);
        _chosenSetting.setOnItemSelectedListener(new SettingSpecSpinner(this));
    }

    public void SetMenuNewCard(View view){
        if(_tags != null) {
            Intent tagMenu = new Intent(this, CreateCardActivity.class);

            if (_allCards != null) {
                String[] toSend = new String[_allCards.length];
                for (int i = 0; i < toSend.length; ++i) {
                    toSend[i] = _allCards[i].toString();
                }
                tagMenu.putExtra("cards", toSend);
            }

            tagMenu.putExtra("tags", _tags);

            startActivity(tagMenu);
        }
        else {
            //TODO Error message, you need to have atleast some tags
        }
    }

    public void SetMenuNewTag(View view){
        /*
        String[] toSend = new String[_allCards.length-1];
        for(int i = 0; i < toSend.length; ++i){
            toSend[i] = _allCards[i].toString();
        }
        */

        Intent cardMenu = new Intent(this, CreateTagActivity.class);
//        cardMenu.putExtra("tags", _tags);
//        cardMenu.putExtra("cards", toSend);
        startActivity(cardMenu);
    }

    public void SetMenuChooseTags(View view){
        try {
            int toTest = Integer.parseInt(_specSettings.getString("DeckSize"));
            if (_specSettings == null) {
                _displayError("No settings found, please restart");
            } else if (_tags == null || _allCards == null) {
                _displayError("either no cards or no tags found, please create some");
            } else if (Integer.parseInt(_specSettings.getString("DeckSize")) > _allCards.length){
                _displayError("not enough cards to make a choice, add more");
            }
            else{
                Intent tagMenu = new Intent(this, SelectTags.class);

                String[] toSend = new String[_allCards.length];
                for (int i = 0; i < toSend.length; ++i) {
                    toSend[i] = _allCards[i].toString();
                }
                tagMenu.putExtra("cards", toSend);

                tagMenu.putExtra("tags", _tags);
                tagMenu.putExtra("settings", _specSettings.toString());
                tagMenu.putExtra("specSetting", openSetting);
                startActivity(tagMenu);
            }
        }
        catch (JSONException e){
            _displayError("settings threw a JSON exception");
        }
    }

    public void SetMenuSettings(View view){
        Intent settingsMenu = new Intent(this, CustomSettings.class);
        startActivity(settingsMenu);
    }

    public void changeSpecSetting(int pos){
            String[] potSpecs = _fileLoader.getPosSpecSettings();
            _specSettings = _fileLoader.loadSpecSettings(potSpecs[pos]);
            _fileLoader.changeSpecSettings(potSpecs[pos]);
            openSetting = pos;
    }

    public void deleteAll(View view){
        try {
            _fileLoader.deleteFile(_specSettings.getString("CardFile"));
            _fileLoader.deleteFile(_specSettings.getString("TagFile"));
        }
        catch (JSONException e){
            _displayError("settings went wrong deleting things");
        }
    }

    private void _displayError(String toDisplay){
        if(toDisplay != null) {
            Toast.makeText(getApplicationContext(), toDisplay, Toast.LENGTH_LONG).show();
        }
    }
}
