package dominion.test.smartdominionbuilder.FindCards;

import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import dominion.test.smartdominionbuilder.Card;
import dominion.test.smartdominionbuilder.CardInput;
import dominion.test.smartdominionbuilder.LoadScreen.LoadCardsActivity;
import dominion.test.smartdominionbuilder.Tuple;
import dominion.test.smartdominionbuilder.cardSorter;
import dominion.test.smartdominionbuilder.invalidList;

/**
 * Created by Anders on 02-04-2018.
 */

public class FindCards extends AsyncTask<CardInput, Tuple<Integer, String>, Card[]> {

    private List<Card> _toUse;
    private List<Card> _reqCards;
    private int _reqProg = 0; //how far along we are with required cards, can't remove due to how we force use tags
    private List<String> _banTags;
    private List<String> _reqTags;
    private int _maxTimeUse;
    private int _currentAmountFoundCards = 0;

    //TODO these should be a part of settings
    private final float _startTemp = 10;
    private final float _tempReduction = 1;
    private final float _annealMaxThreshold = 100;

    private Thread _searchThread;

    private Card[] _toGet = null;

    public CardProgressI progress = null;


    //TODO I think it doesn't properly send signal back to the main thread.

    public void StartSearch(CardInput[] theIn, CardProgressI progresser) {
        progress = progresser;
        StartSearch(theIn);
    }

    public void StartSearch(final CardInput[] theIn) {

        class searchRunnable implements Runnable {
            private CardInput[] _param;

            public searchRunnable(CardInput[] in) {
                _param = in;
            }

            public void run() {
                _toGet = doInBackground(_param);
            }
        }
        _searchThread = new Thread(new searchRunnable(theIn));
        _searchThread.start();
    }


    /*
    Handles all the search
    Finds a single card and then finds a few cards that should go well with it -> a cluster
    Finds clusters until it reaches the desired amount of single cards
     */
    @Override
    protected Card[] doInBackground(CardInput... cardInputs) { //TODO it skips the last card. not sure where it happens
        Card[] returnValue = new Card[cardInputs[0].deckSize];
        String[] remainingTags = null;
        if (cardInputs[0].reqTags != null) {
            //remainingTags is used to see which have been randomly selected, while reqTags is used at the end, don't optimize remainingTags away
            remainingTags = new String[cardInputs[0].reqTags.length];
            System.arraycopy(cardInputs[0].reqCards, 0, remainingTags, 0, cardInputs[0].reqCards.length);
        }

        //TODO make sure to use the required cards as pivots

        for (int i = 0; i < (cardInputs[0].deckSize - (cardInputs[0].deckSize % cardInputs[0].clusterSize)
                / cardInputs[0].clusterSize); i = i + cardInputs[0].clusterSize) {
            if (progress != null) {
                progress.currentProg("Finding Pivot: " + Integer.toString(i));
            }
            if (cardInputs[0].textToUpdate != null) {
                cardInputs[0].textToUpdate.setText("iteration: " + i);
            }
            Card pivot = null;
            //Will try to use required cards as pivots, and if it can't will just find a weighted random card
            if(cardInputs[0].reqCards != null) {
                for (int j = _reqProg; j < cardInputs[0].reqCards.length; ++j) {
                    if (cardInputs[0].reqCards[j] != null) {
                        pivot = _drawCard(cardInputs[0].reqCards[j], cardInputs[0].availableCards);
                        ++_reqProg;
                        break;
                    }
                }
            }
            if(pivot == null){ //incase there aren't any required cards left
                pivot = _FindSingleCard(cardInputs[0].availableCards, remainingTags, cardInputs[0].maxCardUseTime);
            }
            if( (cardInputs[0].deckSize - (cardInputs[0].deckSize % cardInputs[0].clusterSize) / cardInputs[0].clusterSize) == i+1 &&
                    cardInputs[0].deckSize % cardInputs[0].clusterSize == 1) {
                returnValue[cardInputs[0].deckSize -1] = pivot;
            }
            else {
                //        try {
                if (progress != null) {
                    progress.currentProg("Finding Companions: " + Integer.toString(i));
                }
                Card[] singleCluster = _FindNearbyCards(cardInputs[0].clusterSize, pivot,
                        cardInputs[0].maxCardUseTime, cardInputs[0].availableCards, remainingTags);

                returnValue[i] = pivot;
                for (int j = 1; j < cardInputs[0].clusterSize; ++j) {
                    if (i + j < 10) {
                        returnValue[i + j] = singleCluster[j - 1];
                    } else {
                        break;
                    }
                }

                if (remainingTags != null) {
                    remainingTags = _RemoveFoundTags(remainingTags, singleCluster);
                }

                _currentAmountFoundCards += singleCluster.length;
                    /*        } catch (invalidList e) {
                return null;
            } */
            }
        }

        if((remainingTags != null && remainingTags.length > 0 ) ||(cardInputs[0].reqCards != null && cardInputs[0].reqCards.length > _reqProg)){
            if(cardInputs[0].reqCards == null){
                cardInputs[0].reqCards = new Card[0];
            }
            if(remainingTags == null){
                remainingTags = new String[0];
            }
            if(cardInputs[0].reqTags == null){
                cardInputs[0].reqTags = new String[0];
            }
            returnValue = _forceUseReqCards(returnValue, cardInputs[0].reqCards, _reqProg, cardInputs[0].availableCards, remainingTags, cardInputs[0].reqTags, cardInputs[0].clusterSize);
        }



        final Card[] returnedReturn = returnValue; //done because it needs a final value to return



        _currentAmountFoundCards = 0;

        Handler uiThread = new Handler(cardInputs[0].finishContext.getMainLooper());

        class returnCards implements Runnable {
            private LoadCardsActivity _callback;
            private Card[] _returned;

            public returnCards(LoadCardsActivity toCall, Card[] newReturn) {
                _callback = toCall;
                _returned = newReturn;
            }

            public void run() {
                _callback.FoundCards(returnedReturn);
            }
        }

        Thread callBackThread = new Thread(new returnCards(cardInputs[0].finishCall, returnedReturn));
        callBackThread.start();

        return returnedReturn;
    }

    /*
    Chooses a single card at random
    Probability is however favored towards cards which the user rates highly
     */
    @Nullable
    private Card _FindSingleCard(Card[] cardsChooseFrom, String[] requiredTags,
                                 int maxTimeUsed) {
        Pair<Card, Integer>[] probabilityCard = new Pair[cardsChooseFrom.length];
        int currentSum = 0; //total amount of probability

        int iterator = 0;
        for (Card toAdd : cardsChooseFrom) {
            if(toAdd != null) {
                int checkValue = (int) toAdd.indivRate - (maxTimeUsed - toAdd.timeLastUsed);
                if (checkValue > 0) { //sorts out the completely disliked, unless it's been a long time since last usage
                    currentSum += checkValue;
                    probabilityCard[iterator] = new Pair<>(toAdd, currentSum);
                    ++iterator;
                }
            }
        }

        probabilityCard = Arrays.copyOfRange(probabilityCard, 0, iterator);

        Random generator = new Random();
        int chosenRandom;

        if (currentSum == 0) {
            chosenRandom = generator.nextInt(cardsChooseFrom.length - 1);
            Card potTake = cardsChooseFrom[chosenRandom];
            int startPoint = chosenRandom;
            while(potTake == null){
                if(chosenRandom >= cardsChooseFrom.length){
                    chosenRandom = 0;
                }
                else {
                    ++chosenRandom;
                }
                if(chosenRandom == startPoint){
                    throw new invalidList("Not enough cards in the list");
                }
                potTake = cardsChooseFrom[chosenRandom];
            }
            return _drawCard(cardsChooseFrom[chosenRandom], cardsChooseFrom); //if you only have disliked ones
        }

        chosenRandom = generator.nextInt(currentSum);

        for (int i = 0; i < probabilityCard.length; ++i) {
            if (probabilityCard[i].second <= chosenRandom &&
                    (i == probabilityCard.length-1 || probabilityCard[i+1].second > chosenRandom )) {
                return _drawCard( probabilityCard[i].first, cardsChooseFrom);
            }
        }
        throw new invalidList(String.valueOf(chosenRandom));
    }

    /*
    Chooses random card with the probabilities stacked towards those that synergies with distancePlotter
    Remember: if it isn't random enough then it would just find some local optimal, not "new" enough
    If we wanted a local optimal we would just use proper simulated annealing.
     */
    private Card[] _FindNearbyCards(int amountTotal, Card distancePlotter,
                                    int maxTimeUsed, Card[] cardsChooseFrom, String[] reqTags) {
        Tuple<Card, float[]>[] graph = _plotGraph(cardsChooseFrom, distancePlotter);

        Tuple<Card, Float>[] cardsDistanceSorted = _assignDistanceCards(graph);

        Tuple<Card, Float>[] chosenTuples = new Tuple[amountTotal];

        float currentTemp = _startTemp;

        //finds random cards with stacked probabilities
        for (int i = 0; i < amountTotal; ++i) {
            Tuple<Card, Float> potTake = _chooseRandomCard(cardsDistanceSorted);
            if (potTake == null) {
                throw new invalidList("not enough cards had been searched");
            }

            Tuple<Card, Float> potAnnealed = _chooseRandomCard(cardsDistanceSorted);

            if (potAnnealed != null && _Anneal(potTake.second, potAnnealed.second, currentTemp)) {
                currentTemp -= _tempReduction;
                potTake = potAnnealed;
            }

            chosenTuples[i] = potTake;

            for (int j = 0; j < cardsDistanceSorted.length; ++j) {
                if (cardsDistanceSorted[j] != null && cardsDistanceSorted[j].first.name.equals((potTake.first.name))) {
                    cardsDistanceSorted[j] = null;
                    break;
                }
            }

            graph = _plotGraph(cardsChooseFrom, distancePlotter);
            cardsDistanceSorted = _assignDistanceCards(graph);
        }
        /*
        This might seem redundant but it surprisingly isn't
        Annealing stacks probability even more against those with distance
        It also still keeps the random elements
         */
        while (currentTemp > 0) {
            for (int i = 0; i < chosenTuples.length; ++i) {
                Tuple<Card, Float> randomCard = _chooseRandomCard(cardsDistanceSorted);
                if (chosenTuples[i] != null && chosenTuples[i].second != null &&
                        _Anneal(chosenTuples[i].second, randomCard.second, currentTemp)) {
                    Tuple<Card, Float> intermediate = chosenTuples[i];
                    chosenTuples[i] = randomCard;
                    for (int j = 0; j < cardsDistanceSorted.length; ++j) {
                        if (cardsDistanceSorted[j] != null && cardsDistanceSorted[j].first.name.equals(randomCard.first.name)) {
                            cardsDistanceSorted[j] = intermediate;
                            break;
                        }
                    }
                }
                currentTemp -= _tempReduction;
                if (currentTemp <= 0) {
                    break;
                }
            }
        }

        Card[] returnValue = new Card[amountTotal];

        for (int i = 0; i < amountTotal; ++i) {
            returnValue[i] = chosenTuples[i].first;
        }

        /*
        if (reqTags != null) {
            returnValue[amountTotal + 1] = distancePlotter;

            String[] checkTags = new String[reqTags.length];
            System.arraycopy(reqTags, 0, checkTags, 0, reqTags.length);
            checkTags = _RemoveFoundTags(checkTags, returnValue);

            int foundTags = reqTags.length - checkTags.length;
            int requiredAmountTags = ((reqTags.length - 1) - (reqTags.length - 1) % amountTotal) / amountTotal + (reqTags.length - 1) % amountTotal;
            int neededAmountTags = requiredAmountTags - foundTags;
            int[] usedTags = new int[reqTags.length];
            int usedTagsIndex = 0;
            int[] replaceCards = new int[amountTotal];
            int replaceCardsIndex = 0;
            for (int j = 0; j < returnValue.length; ++j) {
                boolean containsUsedTag = false;
                for (int i = 0; i < reqTags.length; ++i) {
                    for (String checkTag : returnValue[i].tags) {
                        if (checkTag.equals(reqTags[i])) {
                            boolean foundTag = false;
                            for (int k = 0; i < usedTagsIndex; ++k) {
                                if (usedTags[k] == i) {
                                    foundTag = true;
                                    break;
                                }
                            }
                            if (!foundTag) {
                                usedTags[usedTagsIndex] = i;
                                ++usedTagsIndex;
                                containsUsedTag = true;
                            }
                        }
                    }
                }
                if (!containsUsedTag) {
                    replaceCards[replaceCardsIndex] = j;
                    ++replaceCardsIndex;
                }
            }

            for (int toReplace : replaceCards) {
                //here it should find the ones that need to be replaced
                //choose a unused tag and choose the card nearest to pivot with the tag
                boolean foundCard = false;
                for (Tuple<Card, Float> toCheck : cardsDistanceSorted) {
                    for (String checkTag : toCheck.first.tags) {
                        for (int i = 0; i < checkTags.length; ++i) {
                            if (checkTags[i] != null && checkTags[i].equals(checkTag)) {
                                foundCard = true;
                                checkTags[i] = null;
                                returnValue[toReplace] = toCheck.first;
                                break;
                            }
                        }
                    }
                }
                if (!foundCard) {
                    throw new invalidList("Cannot resolve tag");
                }
            }
        }
        */

        //remove used cards from cardsChooseFrom
        for(Card toRemove : returnValue){
            _drawCard(toRemove, cardsChooseFrom);
        }

        return returnValue;
    }

    private Tuple<Card, float[]>[] _plotGraph(Card[] cardsChooseFrom, Card distancePlotter) {
        Tuple<Card, float[]>[] graph = new Tuple[cardsChooseFrom.length];

        for (int j = 0; j < cardsChooseFrom.length; ++j) {
            if (cardsChooseFrom[j] != null) {
                Tuple<Card, float[]> toAdd = new Tuple<>();
                toAdd.first = cardsChooseFrom[j];
                toAdd.second = new float[cardsChooseFrom[j].tagSynergy.length];
                short actualSize = 0;
                for (int i = 0; i <= distancePlotter.tagSynergy.length; ++i) {
                    for (Tuple<String, Float> checkTag : distancePlotter.tagSynergy) {
                        if(checkTag != null && checkTag.first != null) {
                            for (String potInCardTag : cardsChooseFrom[j].tags) {
                                if (cardsChooseFrom[j].tags.length > i && cardsChooseFrom[j].tags[i].equals(checkTag.first)) {
                                    toAdd.second[i] = checkTag.second;
                                    ++actualSize;
                                    break;
                                }
                            }
                        }
                    }
                }
                if(actualSize > 0) {
                    toAdd.second =  Arrays.copyOfRange(toAdd.second, 0, actualSize);
                    graph[j] = toAdd;
                }
            }
        }
        return graph;
    }
    /*
    assigns the "distance", the probability of choosing each card, based on the synergy
    Please note that the average distance of card[i] is card[i].second - x[i-1].second
     */
    private Tuple<Card, Float>[] _assignDistanceCards(Tuple<Card, float[]>[] toSort) {
//averages the distances, is what will be sorted from
        Tuple<Card, Float>[] returnValue = new Tuple[toSort.length];

        float currentDistance = 0;

        for (int i = 0; i < toSort.length; ++i) {
            if(toSort[i] == null){
                returnValue[i] = null;
                continue;
            }
            float singleDistance = 0;
            short j = 0;
            for (j = 0; j < toSort[i].second.length; ++j) {
                singleDistance += toSort[i].second[j];
            }
            if (singleDistance > 0 && j > 0) {
                singleDistance = singleDistance / j;
                currentDistance += singleDistance;
                Tuple toAdd = new Tuple<>(toSort[i].first,  currentDistance);
                toAdd.sorter = new cardSorter<>();
                returnValue[i] = toAdd;
            }
            else {
                returnValue[i] = null;
            }
        }

        return returnValue;
    }

/*
    This is the actual selection based on distance.
     probabilities are stacked based on the float
 */
    private Tuple<Card, Float> _chooseRandomCard(Tuple<Card, Float>[] cardsChooseFrom) {
        float totalDistance = 0;

        for(int i = cardsChooseFrom.length-1; i > 0; --i){
            if(cardsChooseFrom[i] != null){
                totalDistance = cardsChooseFrom[i].second;
                break;
            }
        }
        //totalDistance = cardsChooseFrom[cardsChooseFrom.length-1].second;

        Random generator = new Random();
        float chosenRandom = generator.nextFloat() * totalDistance;
        for (int j = 0; j < cardsChooseFrom.length; ++j) {
                if (cardsChooseFrom[j] != null && cardsChooseFrom[j].second != null &&
                        cardsChooseFrom[j].second >= chosenRandom ) {
                    return cardsChooseFrom[j];
                }
        }
        return null;
    }

    private boolean _Anneal(float distanceCurrent, float distancePot, float temperature) {
        if (temperature <= 0) {
            return false;
        }
        Random generator = new Random();
        return Math.exp(((distanceCurrent - distancePot) / temperature)) > _annealMaxThreshold * generator.nextFloat();
    }

    /*
    Sorts out required tags which have already been used and removes them from remainingTags
     */
    private String[] _RemoveFoundTags(String[] tagsToCheck, Card[] toCheck) {
        int[] toSkip = new int[tagsToCheck.length];
        int skipIndex = 0;
        for (Card checkCard : toCheck) {
            for (String checkCardTag : checkCard.tags) {
                for (int i = 0; i < tagsToCheck.length; ++i) {
                    if (tagsToCheck[i].equals(checkCardTag)) {
                        toSkip[skipIndex] = i;
                        ++skipIndex;
                    }
                }
            }
        }
        if (skipIndex > 0) {
            String[] returnValue = new String[tagsToCheck.length - skipIndex];
            int addIndex = 0;
            for (int i = 0; i < tagsToCheck.length; ++i) {
                boolean willBeSkipped = false;

                for (int potSkip : toSkip) {
                    if (potSkip == i) {
                        willBeSkipped = true;
                        break;
                    }
                }
                if (!willBeSkipped) {
                    returnValue[addIndex] = tagsToCheck[i];
                    ++addIndex;
                }
            }
            return returnValue;
        } else {
            return tagsToCheck;
        }
    }

    /*
    This needs to do both cards and tags
     */
    private Card[] _forceUseReqCards(Card[] toCheck, Card[] remainingReqCard, int currentProg,
                                     Card[] deck, String[] remainingReqTags, String[] allReqTags, int clusterSize){
        List<Integer> potReplace = new ArrayList<Integer>();
        //Prepare amountUsedReqTags to count how many times these tags are used
        Tuple<String, Integer>[] amountUsedReqTags = new Tuple[allReqTags.length];
        for(int i = 0; i < amountUsedReqTags.length; ++i){
            amountUsedReqTags[i] = new Tuple<>(allReqTags[i], 0);
        }

            //First sort out which cards we are allowed to replace, based on cards
        for(int j = 0; j < toCheck.length; ++j){
            if((j+1) % clusterSize != 1){ //we assume pivots are always
                boolean foundCheck = false;
                for(Card checkIfReq : remainingReqCard){ //Make sure we aren't actually using a required card through accident
                    if(checkIfReq.name.equals(toCheck[j])){
                        foundCheck = true;
                        break;
                    }
                }
                if(!foundCheck){
                    potReplace.add(j);
                }
            }
            //Also count how many times the required tags are used, so that we don't accidentally remove some of them
            for(String toCheckTag : toCheck[j].tags){
                for(Tuple<String, Integer> toCheckTagAgainst : amountUsedReqTags){
                    if(toCheckTag.equals(toCheckTagAgainst.first)){
                        ++toCheckTagAgainst.second;
                    }
                }
            }
        }


        //Now we try and see if we can replace them. We do this 1 by 1 i fear a perfect solution would be too slow
        //First Integer is pos in remainingReqCard, Float is how alike it is, second Integer is how many unused required tags it contains - how many required tags we would lose
        boolean allTagsFulfilled = false;
        boolean allCardsFulfilled = false;
        if(allReqTags.length == 0){
            allTagsFulfilled = true;
        }
        //if both were true the method shouldn't even be called
        else if(remainingReqCard.length == 0){
            allCardsFulfilled = true;
        }
        for(int i = 0; i < potReplace.size(); ++i){
            Tuple<Integer, Tuple<Float, Integer>>[] measuredReplacements = new Tuple[remainingReqCard.length];
            for(int k = 0; k < remainingReqCard.length; ++k) {
                measuredReplacements[k] = new Tuple<Integer, Tuple<Float, Integer>>(k, new Tuple<Float, Integer>(0f, 0));
                //Measure how many required tags we would fulfill, compared to how many we would lose
                if(!allTagsFulfilled) {
                    //How many required tags it would fulfill
                    for (int j = 0; j < remainingReqTags.length; ++j) {
                        for (String potReqTag : remainingReqCard[k].tags) {
                            if (potReqTag.equals(remainingReqTags[j])) {
                                ++measuredReplacements[i].second.second;
                            }
                        }
                    }
                }
                //How many required tags we might potentially lose. Assumes all tags are equally bad to lose
                for (int j = 0; j < amountUsedReqTags.length; ++j) {
                    for (String potReqTag : toCheck[potReplace.get(i)].tags) {
                        if (amountUsedReqTags != null && potReqTag.equals(amountUsedReqTags[j].first)) {
                            --measuredReplacements[i].second.second;
                        }
                    }
                }
                measuredReplacements[k].second.first = _likenes(toCheck[potReplace.get(i)], remainingReqCard[k]);
            }
            //Find the best of these choices, Prioritises the tags it might displace
            Tuple<Integer, Tuple<Float, Integer>> bestChoice = measuredReplacements[0];
            for (int j = 1; j < measuredReplacements.length; ++j) {
                    if (bestChoice.second.second < measuredReplacements[j].second.second ||
                            (bestChoice.second.second == measuredReplacements[j].second.second && bestChoice.second.first < measuredReplacements[j].second.first)) {
                        bestChoice = measuredReplacements[j];
                    }

            }
            //Update the remainingTags list and the amountUsedReqTags list. If remainingTags is empty, set allTagsFulfilled as true
            for (int j = 0; j < remainingReqTags.length; ++j) {
                for (String potReqTag : remainingReqCard[bestChoice.first].tags) {
                    if (potReqTag.equals(remainingReqTags[j])) {
                        if(remainingReqTags.length == 1){
                            allTagsFulfilled = true;
                        }
                        else {
                            String[] newRemaingReqTags = new String[remainingReqTags.length - 1];
                            int mod = 0;
                            for (int k = 0; k < remainingReqTags.length; ++k) {
                                if (k == j) {
                                    mod = 1;
                                } else {
                                    newRemaingReqTags[k - mod] = remainingReqTags[k];
                                }
                            }
                        }
                    }
                }
            }
            for (int j = 0; j < amountUsedReqTags.length; ++j) {
                for (String potReqTag : toCheck[potReplace.get(i)].tags) {
                    if (potReqTag.equals(amountUsedReqTags[j].first)) {
                        --amountUsedReqTags[j].second;
                        if(amountUsedReqTags[j].second == 0){
                            boolean notFound = true;
                            for(int k = 0; k < remainingReqTags.length; ++k){
                                if(remainingReqTags[k] == null){
                                    notFound = false;
                                    remainingReqTags[k] = amountUsedReqTags[j].first;
                                    break;
                                }
                            }
                            if(notFound){
                                String[] newRemainingReqTags = new String[remainingReqTags.length+1];
                                System.arraycopy(remainingReqTags, 0, newRemainingReqTags, 0, remainingReqTags.length);
                                newRemainingReqTags[newRemainingReqTags.length] = amountUsedReqTags[j].first;
                                remainingReqTags = newRemainingReqTags;
                            }
                            amountUsedReqTags[j] = null;
                        }
                    }
                }
            }
            if(!allCardsFulfilled) {
                //replaces the card, i think this is faster than using draw(Card, ...) and putBack because i need the index in both
                Card shuffleBack = toCheck[potReplace.get(i)];
                for (int j = 0; j < deck.length; ++j) {
                    if (remainingReqCard[bestChoice.first].name.equals(deck[j])) {
                        toCheck[potReplace.get(i)] = _drawCard(j, deck);
                        deck[j] = shuffleBack;
                    }
                }
                //TODO use a list for remainingReqCard and just shrink it instead
                boolean notFound = true;
                for(int j = 0; j < remainingReqCard.length; ++j){
                    if(remainingReqCard[j] != null){
                        notFound = false;
                        break;
                    }
                }
                allCardsFulfilled = notFound;
            }
            else if(!allTagsFulfilled){
                /*
                A perfect search would take too long, so we only take some of it
                 */
                int bestCard = -1;
                int highestNewTags = 0;
                Random rand = new Random();
                for(int j = rand.nextInt(3); j < deck.length*0.25f; ++j){
                    if(deck.length < j*4 && deck[j*4] != null){
                        int amountAddedReqTags = 0;
                        for(String tagCheck : deck[j*4].tags){
                            for(int k = 0; k < amountUsedReqTags.length; ++k){
                                if(amountUsedReqTags[k].second == 1 && amountUsedReqTags[k].first.equals(tagCheck)){
                                    --amountAddedReqTags;
                                }
                            }
                            for(int k = 0; k < remainingReqTags.length; ++k){
                                if(remainingReqTags[k] != null && remainingReqTags[k].equals(tagCheck)){
                                    ++amountAddedReqTags;
                                }

                            }

                            if(bestCard == -1){
                                if(amountAddedReqTags > 0) {
                                    bestCard = j;
                                }
                            }
                            else if( highestNewTags < amountAddedReqTags) {
                                bestCard = j*4;
                            }
                            else if(highestNewTags == amountAddedReqTags && _likenes(deck[j*4], toCheck[potReplace.get(i)]) > _likenes(deck[bestCard], toCheck[potReplace.get(i)]) ){
                                bestCard = j*4;
                            }
                        }
                        //Draw the new card
                        if(bestCard != -1){
                            Card replacedCard =toCheck[potReplace.get(i)];
                            toCheck[potReplace.get(i)] = _drawCard(j, deck);
                            _putBack(replacedCard, deck);
                        }

                    }
                }

            }

        }

        if(!allCardsFulfilled || !allTagsFulfilled) {
            //TODO throw an error if there are still any remaining required tags or required cards
        }

        return toCheck;
    }

    /*
    Measures how much the card resembles another
     */
    private Float _likenes(Card a, Card b){
        if(a.name.equals(b.name)){
            return 1f;
        }
        float returnValue = 0;
        for(String tagA : a.tags){
            for(String tagB : b.tags){
                if(tagA.equals(tagB)){
                    ++returnValue;
                }
            }
        }

        returnValue = returnValue / ( (a.tags.length+b.tags.length )*0.5f);

        return returnValue;
    }


    private Card[] _putBack(Card toPutBack, Card[] deck){
        for(int i = 0; i < deck.length; ++i){
            if(deck[i] == null){
                deck[i] = toPutBack;
                return deck;
            }
        }
        Card[] newDeck = new Card[deck.length+1];
        System.arraycopy(deck, 0, newDeck, 0, deck.length);
        newDeck[newDeck.length] = toPutBack;
        return newDeck;
    }

    private Card _drawCard(int Index, Card[] Deck){
        Card returnValue = Deck[Index];

        Deck[Index] = null;

        return returnValue;
    }

    private Card _drawCard(Card toDraw, Card[] Deck){
        for(int i = 0; i < Deck.length; ++i){
            if(Deck[i] != null && toDraw.name.equals(Deck[i].name)){
                return _drawCard(i, Deck);
            }
        }
        return null;
    }

    private Card[] _removeNulls(Card[] removedFrom){
        int size = removedFrom.length;
        for(Card check : removedFrom){
            if(check == null){
                size -= 1;
            }
        }
        Card[] returnValue = new Card[size];
        int j = 0;
        for(Card add : removedFrom){
            if(add != null){
                returnValue[j] = add;
                ++j;
            }
        }
        return returnValue;
    }
}
