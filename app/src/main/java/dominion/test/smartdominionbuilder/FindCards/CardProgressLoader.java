package dominion.test.smartdominionbuilder.FindCards;

import dominion.test.smartdominionbuilder.LoadScreen.LoadCardsActivity;

public class CardProgressLoader implements CardProgressI {
    LoadCardsActivity toSend;

    public void currentProg(String toGive){
        if(toSend != null && toSend.getClass().equals(LoadCardsActivity.class)){
            ((LoadCardsActivity)toSend).UpdateText(toGive);
        }
    }

    public void loadReturner(Object toSendIn){
        if(toSend != null && toSend.getClass().equals(LoadCardsActivity.class)){
            toSend = (LoadCardsActivity) toSendIn;
        }
    }

}
