package dominion.test.smartdominionbuilder.SettingsMenu;

import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

public class settingListItem {
    public String settingElement; //name of the element, also what changes
    public boolean isString; //how to change it
    public String setting; //If it got a starting value, set this to it.
    public EditText potStringIn;
    public PotTypeChange whatChange; //TODO figure out what this was supposed to do, is it just isString?

    public JSONObject toJSON(){
        try{
            JSONObject returnValue = new JSONObject();
            returnValue.put("setE", settingElement);
            returnValue.put("type", isString);
            returnValue.put("set", setting);
            if(potStringIn != null) {
                String toTest = potStringIn.getText().toString();
                returnValue.put("assignedValue", potStringIn.getText().toString());
            }
            return returnValue;
        }
        catch(JSONException e){
            return null;
        }
    }

    public settingListItem(JSONObject source){
        try{
            settingElement = source.getString("setE");
            isString = source.getBoolean("type");
            setting = source.getString("set");
        }
        catch (JSONException e){

        }
    }

    public settingListItem(String toChange, boolean inputIsString, String startingValue, PotTypeChange typeToChange){
        settingElement = toChange;
        isString = inputIsString;
        setting = startingValue;
        whatChange = typeToChange;
    }

    @Override
    public String toString(){
        return toJSON().toString();
    }
}
