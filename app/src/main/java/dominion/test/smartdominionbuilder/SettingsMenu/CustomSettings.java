package dominion.test.smartdominionbuilder.SettingsMenu;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import dominion.test.smartdominionbuilder.FileLoader;
import dominion.test.smartdominionbuilder.MainMenu;
import dominion.test.smartdominionbuilder.R;
import dominion.test.smartdominionbuilder.SettingSpecSpinner;
import dominion.test.smartdominionbuilder.Tuple;

public class CustomSettings extends AppCompatActivity {

    //public settingListAdapter genAdapter;
    public settingListAdapter specAdapter;
    //public settingListItem[] genSettings;
    public settingListItem[] specificSettings;
    public RecyclerView general;
    public RecyclerView specific;

    public Context mainContext;
    public String[] possibleSpecs;
    public List<Tuple<String, JSONObject[]>> tempSavedSpecs; //using settingListItem would lead to not having a editText
    public Spinner chosenSpec;
    public ArrayAdapter<String> spinnerAdapter;

    private TextView _newSetting;

    private FileLoader _loader;

    private int _currentSpec = 0;

    /*Should have 2 settings
    first is general settings: Not entirely sure what this should be, but might need later
    second is specific file settings: Every file should have their own settings, changeable in the menu
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        tempSavedSpecs = new ArrayList<>();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_settings);
        mainContext = getApplicationContext();
        _newSetting = findViewById(R.id.new_setting_name);

        _loader = new FileLoader(getApplicationContext());
        try {
            JSONArray loadPossibleSpec = _loader.getBaseSettings().getJSONArray("SpecSets");
            possibleSpecs = _loader.getPosSpecSettings();
        }
        catch(JSONException e){
            //Unsure what to do here, maybe go back and call it and error?
        }
        chosenSpec = findViewById(R.id.dropSpecSet);
        spinnerAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_dropdown_item, possibleSpecs);
        chosenSpec.setAdapter(spinnerAdapter);
        chosenSpec.setOnItemSelectedListener(new SettingSpecSpinner(this));

        //genSettings = setListData(_loader.getBaseSettings());
        specificSettings = setListData(_loader.loadSpecSettings(possibleSpecs[0]));

        createMenu();
    }

    @Nullable
    public settingListItem[] setListData(JSONObject source){
        try {
            JSONArray allElements = source.getJSONArray("elements");
            settingListItem[] testSet = new settingListItem[allElements.length()];
            for(int i = 0; i < allElements.length(); ++i){
                testSet[i] = new settingListItem(allElements.getJSONObject(i));
            }
            return testSet;
        }
        catch (JSONException e){
            return null;
        }
    }

    public void createMenu(){
        //general = findViewById(R.id.genSet);
        specific = findViewById(R.id.specSet);
        //genAdapter = new settingListAdapter(genSettings);
        specAdapter = new settingListAdapter(specificSettings);

        //LinearLayoutManager genLayMan = new LinearLayoutManager(getApplicationContext());
        LinearLayoutManager specLayMan = new LinearLayoutManager(getApplicationContext());
        //general.setLayoutManager(genLayMan);
        specific.setLayoutManager(specLayMan);
        //general.setItemAnimator(new DefaultItemAnimator());
        specific.setItemAnimator(new DefaultItemAnimator());
        //general.setAdapter(genAdapter);
        specific.setAdapter(specAdapter);

        //genAdapter.notifyDataSetChanged();
        specAdapter.notifyDataSetChanged();
    }

    public void setNewSpecList(int pos){
        tempSaveSpec();
        specificSettings = setListData(_loader.loadSpecSettings(possibleSpecs[pos]));
        specAdapter.changeContent(specificSettings);
        specAdapter.notifyDataSetChanged();
        _currentSpec = pos;
    }

    /*
    Use just to save the curent SPECific setting
    Should only be properly saved at finish
     */
    public void tempSaveSpec(){
        JSONObject[] allSaved = new JSONObject[specificSettings.length];
        for(int i = 0; i < allSaved.length; ++i){ //TODO I'm pretty sure the problem is here, the json doesn't get the proper value
            allSaved[i] = specificSettings[i].toJSON();
        }
        Tuple<String, JSONObject[]> tempSpec = new Tuple<>(possibleSpecs[_currentSpec], allSaved);

        tempSavedSpecs.add(tempSpec);
    }

    public void createNewSetting(View view){
        if(_newSetting == null){
            return;
        }
        String toMake = _newSetting.getText().toString();
        _loader.createSpecSettings(toMake);
        possibleSpecs = _loader.getPosSpecSettings();
        setNewSpecList(possibleSpecs.length-1);
        spinnerAdapter.notifyDataSetChanged(); //TODO this doesn't work, but can't be bothered to fix, probably because possibleSpecs points to a different string array now, it should have been a pointer to a pointer internally
    }

    public void finish(View view){
        tempSaveSpec();
        for(Tuple<String, JSONObject[]> toSave : tempSavedSpecs){
            _loader.saveSettings(_converterSettingItemJSON(toSave.first, toSave.second), toSave.first);
        }
        cancel(view); //better than rewriting the same part twice
    }

    private JSONObject _converterSettingItemJSON(String toOverwrite, JSONObject[] actualSettings){
        JSONObject originalSpec = _loader.loadSpecSettings(toOverwrite);
        JSONObject orignalSpecCopy = null;
        try {
            orignalSpecCopy = new JSONObject(originalSpec.toString());
        }
        catch (JSONException e){
            //TODO orignalSpec is corrupt, delete it
        }
        for(int i = 0; i < actualSettings.length; ++i){
            String toSaveString = null, usedSetting;
            boolean toSaveBool = false, boolAssigned = false, removed = false;
            //TODO It only changes the actual value, not the menu elements
            try{ //Setting it up to catch dumb shit before making bad mistakes
                usedSetting = actualSettings[i].getString("setE");
                if(actualSettings[i].getBoolean("type")){
                    toSaveString = originalSpec.getString(usedSetting);
                }
                else {
                    toSaveBool = originalSpec.getBoolean(usedSetting);
                    boolAssigned = true;
                }
            }
            catch (JSONException e){
                continue; //Keep the original value for this one
            }
            try{
                originalSpec.remove(usedSetting);
                removed = true;
                originalSpec.put(usedSetting, actualSettings[i].get("assignedValue"));
            }
            catch (JSONException e){ //this is where we fix any problems, with data from the first try
                if(usedSetting != null && removed){
                    try {
                        if (toSaveString != null) {
                            originalSpec.remove(usedSetting);
                            originalSpec.put(usedSetting, toSaveString);
                        } else if (boolAssigned) {
                            originalSpec.remove(usedSetting);
                            originalSpec.put(usedSetting, toSaveBool);
                        }
                    }
                    catch (JSONException ee){
                        //TODO figure out what to do here, probably assume file is corrupt
                    }
                }
            }
        }
        JSONArray newMenuElements = new JSONArray();
        try{
            originalSpec.remove("elements");
            for(int i = 0; i < actualSettings.length; ++i){
                actualSettings[i].remove("set");
                actualSettings[i].put("set", actualSettings[i].get("assignedValue"));
                actualSettings[i].remove("assignedValue"); //Don't need it at this point
                newMenuElements.put(actualSettings[i]);
            }
            originalSpec.put("elements", newMenuElements);
        }
        catch (JSONException e){
            boolean test = false;
            return orignalSpecCopy; //The menu elements are fucked, hope it's just a weird glitch
        }
        return originalSpec;
    }
    /*
    returns to main menu without saving anything
     */
    public void cancel(View view){
        Intent intent = new Intent(this, MainMenu.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
