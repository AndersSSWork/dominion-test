package dominion.test.smartdominionbuilder.SettingsMenu;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import dominion.test.smartdominionbuilder.R;

public class settingListAdapter extends RecyclerView.Adapter<settingListAdapter.settingsViewHolder> {

    private settingListItem[] _allListItems;
    public class settingsViewHolder extends RecyclerView.ViewHolder{
        public int owner;
        public boolean isString;
        public String setting;
        public Object setter;
        public EditText toSetPotString;
        public TextView displayedName;

        public settingsViewHolder(View view, boolean isStringIn){
            super(view);
            this.setIsRecyclable(false); //TODO this isn't very efficient, save the value when recycle instead, also in all other lists
            isString = isStringIn;
            if(!isString){
                displayedName = view.findViewById(R.id.setting_name_bool);
                setter = view.findViewById(R.id.setting_setter_bool);
                //TODO if this doesn't work, put it in onBindViewHolder
                if( setting == "T" && !((CheckBox) setter).isChecked()){
                    ((CheckBox) setter).setChecked(true);
                }
                else if(setting == "F" && ((CheckBox) setter).isChecked()){
                    ((CheckBox) setter).setChecked(false);
                }
                ((CheckBox)setter).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(((CheckBox) setter).isChecked()){
                            setting = "T";
                        }
                        else {
                            setting = "F";
                        }
                    }
                });
            }
            else {
                displayedName = view.findViewById(R.id.setting_name_string);
                toSetPotString = (EditText) view.findViewById((R.id.setting_setter_string));
            }
        }


        @Override
        public String toString(){
            return displayedName.toString();
        }
    }

    public settingListAdapter(settingListItem[] input){
        _allListItems = input;
    }

    public void changeContent(settingListItem[] newInput){
        _allListItems = newInput;
    }

    public int getItemViewType(int position){
        return _allListItems[position].isString == true ? 0 : 1;
    }

    @Override
    public settingListAdapter.settingsViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        int chosenLayout = viewType == 0 ? R.layout.setting_element_string : R.layout.setting_element_bool;
        View itemView= itemView = LayoutInflater.from(parent.getContext()).inflate(chosenLayout, parent, false);
        return new settingListAdapter.settingsViewHolder(itemView, viewType == 0);
    }

    @Override
    public void onBindViewHolder(settingListAdapter.settingsViewHolder holder, int pos){
        if(pos < _allListItems.length){
            holder.displayedName.setText(_allListItems[pos].settingElement);
            holder.isString = _allListItems[pos].isString;
            holder.setting = _allListItems[pos].setting;
            if(holder.toSetPotString != null) {
                holder.toSetPotString.setText(holder.setting);
                _allListItems[pos].potStringIn = holder.toSetPotString;
            }
        }
    }

    @Override
    public int getItemCount(){
        if(_allListItems == null){
            return 0;
        }
        return _allListItems.length;
    }
}
