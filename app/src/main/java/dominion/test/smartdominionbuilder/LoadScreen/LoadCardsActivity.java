package dominion.test.smartdominionbuilder.LoadScreen;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.content.Context;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import dominion.test.smartdominionbuilder.Card;
import dominion.test.smartdominionbuilder.CardInput;
import dominion.test.smartdominionbuilder.FileLoader;
import dominion.test.smartdominionbuilder.FindCards.CardProgressI;
import dominion.test.smartdominionbuilder.FindCards.CardProgressLoader;
import dominion.test.smartdominionbuilder.FindCards.FindCards;
import dominion.test.smartdominionbuilder.FinishMenu.FinishActivity;
import dominion.test.smartdominionbuilder.R;

public class LoadCardsActivity extends AppCompatActivity {

    private FindCards _finder;
    private final int _maxCardUseTimes = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_load_cards);
        StartSearching();
    }

    public void StartSearching(){
        _finder = new FindCards();
        CardProgressI progresser = new CardProgressLoader();
        progresser.loadReturner(this);

        CardInput theIn = new CardInput();
        Intent intent = getIntent();

        String[] allTags = intent.getStringArrayExtra("tags");
        String[] cardStrings = intent.getStringArrayExtra("cards");

        Card[] allCards = new Card[cardStrings.length];
        List<Card> mustCards = new ArrayList<Card>();
        String[] banCards = intent.getStringArrayExtra("banCard");
        String[] mustCardsString = intent.getStringArrayExtra("mustCard");
        String[] banTags = intent.getStringArrayExtra("banTag");
        for(int i = 0; i < cardStrings.length; ++i){
            Card newCard = new Card(cardStrings[i], allTags);
            boolean isBan = false;
            if(banCards != null) {
                outerLoop:
                for (String toCheck : banCards) {
                    if (toCheck.equals(newCard.name)) {
                        allCards[i] = null;
                        isBan = true;
                        break;
                    }
                    else {
                        for(String toCheckTag : newCard.tags){
                            for(String toCheckAgainst : banTags){
                                if(toCheckAgainst.equals(toCheckTag)){
                                    allCards[i] = null;
                                    isBan = true;
                                    break outerLoop;
                                }
                            }
                        }
                    }
                }
            }
            if(!isBan) {
                if(mustCardsString != null) {
                    for (String toCheck : mustCardsString) {
                        if (toCheck.equals(newCard.name)) {
                            mustCards.add(newCard);
                            break;
                        }
                    }
                }
                allCards[i] = newCard;
            }
        }


        theIn.availableCards = allCards;
        if(mustCards != null && mustCards.size() > 0) {
            theIn.reqCards = (Card[]) mustCards.toArray(new Card[0]);  //mustCards.toArray can't be cast
        }
        theIn.reqTags = intent.getStringArrayExtra("mustTag");
        theIn.maxCardUseTime = _maxCardUseTimes;
        theIn.finishContext = this;
        theIn.finishCall = this;

        JSONObject settings = null;
        FileLoader loader = new FileLoader(getApplicationContext());
        try{
            loader.changeSpecSettings(loader.getPosSpecSettings()[intent.getIntExtra("specSetting", 3)]);
            settings = loader.getCurrentSpecSettings();
            theIn.clusterSize = Integer.parseInt(settings.getString("ClusterSize"));
            theIn.deckSize = Integer.parseInt(settings.getString("DeckSize"));
        }
        catch (JSONException e){ //defaults for the values.
            theIn.clusterSize = 3;
            theIn.deckSize = 10;
        }

        CardInput[] toGive = new CardInput[1];
        toGive[0] = theIn; //TheIn is null

        _finder.StartSearch(toGive);
    }

    public void UpdateText(String newText){
        ((TextView)findViewById(R.id.progress)).setText(newText);
    }

    public void FoundCards(Card[] theFound){
        _finder = null;

        Intent intent = new Intent(this, FinishActivity.class);
        String[] cardStrings = new String[theFound.length];
        for(int i = 0; i < theFound.length; ++i){
            cardStrings[i] = theFound[i].toString();
        }
        intent.putExtra("cards", cardStrings);
        startActivity(intent);
    }

    @Override
    public void onDestroy(){
        if(_finder != null){
            _finder.cancel(true);
        }
        super.onDestroy();
    }
}
