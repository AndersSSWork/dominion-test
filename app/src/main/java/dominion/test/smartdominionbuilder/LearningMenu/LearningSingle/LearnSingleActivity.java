package dominion.test.smartdominionbuilder.LearningMenu.LearningSingle;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.Random;

import dominion.test.smartdominionbuilder.Card;
import dominion.test.smartdominionbuilder.Learner;
import dominion.test.smartdominionbuilder.MainMenu;
import dominion.test.smartdominionbuilder.R;

public class LearnSingleActivity extends AppCompatActivity {

    private String[] _cardStrings;
    private Card[] _cards;
    private String[] _tags;

    private int _chosenIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_learn_single);

        Intent intent = getIntent();
        _cardStrings = intent.getStringArrayExtra("cards");
        _tags = intent.getStringArrayExtra("tags");
        _cards = new Card[_cardStrings.length];
        for(int i = 0; i < _cards.length; ++i){
            _cards[i] = new Card(_cardStrings[i], _tags);
        }
        Random rand = new Random();
        _chosenIndex = rand.nextInt(_cardStrings.length);
        Card chosenCard = _cards[_chosenIndex];
        ((TextView)findViewById(R.id.cardText)).setText(chosenCard.name);
    }
/*
    public void AllGood(View view){
        _allGood = true;
    }

    public void AllBad(View view){
        _allGood = false;
    }

    public void WasBad(View view){
        _singleGood = false;
    }

    public void WasGood(View view){
        _singleGood = true;
    }
*/
    public void JudgeCard(View view){
        CheckBox singleGood = findViewById(R.id.SingleCardGoodCheck);
        CheckBox allGood  = findViewById(R.id.SingleAllGoodCheck);

        Learner theLearn = new Learner();
        theLearn.JudgeAll(_cards, allGood.isChecked());
        theLearn.JudgeSingle(_cards[_chosenIndex], singleGood.isChecked());
        Intent intent = new Intent(this, MainMenu.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("learnedCards", _cardStrings);
        startActivity(intent);
    }
}
