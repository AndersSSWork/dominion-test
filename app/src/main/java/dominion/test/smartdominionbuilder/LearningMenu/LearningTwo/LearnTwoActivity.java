package dominion.test.smartdominionbuilder.LearningMenu.LearningTwo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.Random;

import dominion.test.smartdominionbuilder.Card;
import dominion.test.smartdominionbuilder.Learner;
import dominion.test.smartdominionbuilder.MainMenu;
import dominion.test.smartdominionbuilder.R;

public class LearnTwoActivity extends AppCompatActivity {

    private String[] _cardStrings;
    private Card[] _cards;
    private int toChooseOne, toChooseTwo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_learn_two);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        _cardStrings = intent.getStringArrayExtra("cards");
        _cards = new Card[_cardStrings.length];

        String[] tags = intent.getStringArrayExtra("tags");

        for(int i = 0; i < _cardStrings.length; ++i){
            _cards[i] = new Card(_cardStrings[i], tags);
        }

        Random random = new Random();
        toChooseOne = random.nextInt(_cards.length);
        toChooseTwo = random.nextInt(_cards.length);
        if(toChooseOne == toChooseTwo){
            if(toChooseTwo == 0){
                toChooseTwo = random.nextInt(_cards.length-1)+1;
            }
            else if(toChooseTwo == _cards.length-1){
                toChooseTwo = random.nextInt(_cards.length-1);
            }
            else {
                toChooseTwo = random.nextFloat() > 0.5 ? random.nextInt(toChooseTwo) : random.nextInt(_cards.length - toChooseTwo) + toChooseTwo;
            }
        }

        TextView nameCard = (TextView)findViewById(R.id.nameFirstLearn);
        nameCard.setText(_cards[toChooseOne].name);
        nameCard = (TextView)findViewById(R.id.nameSecondLearn);
        nameCard.setText(_cards[toChooseTwo].name);
    }

    public void SendResultsToMain(View view){
        CheckBox allGood = findViewById(R.id.allCheckLearn);
        CheckBox comboGood = findViewById(R.id.comboCheckLearn);

        Card[] synCards = new Card[2];
        synCards[0] = _cards[toChooseOne];
        synCards[1] = _cards[toChooseTwo];

        Learner learner = new Learner();
        learner.JudgeSyn(synCards, comboGood.isChecked());
        learner.JudgeAll(_cards, allGood.isChecked());

        Intent intent = new Intent(this, MainMenu.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("learnedCards", _cardStrings);
        startActivity(intent);
    }

}
