package dominion.test.smartdominionbuilder;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import dominion.test.smartdominionbuilder.SettingsMenu.PotTypeChange;
import dominion.test.smartdominionbuilder.SettingsMenu.settingListItem;

public class FileLoader {

    public Context context;
    private JSONObject _baseSettings;
    private JSONObject _specSetting;
    private String _currentOpenSetting = "Dominion";
    public String[] tags;
    private final String _fileSettings = "DomSet"; //default base
    private final String _possibleSpec = "Dominion"; //default specific

    public FileLoader(Context newContext){
        context = newContext;
        try{
            _baseSettings = _loadGenSettings();
            _specSetting = loadSpecSettings(_possibleSpec);
            if(_specSetting == null){
                createSpecSettings(_possibleSpec);
                _specSetting = loadSpecSettings(_possibleSpec);
            }
        }
        catch (JSONException e){
            _baseSettings = null;
        }
    }

    @Nullable
    public Card[] LoadCards() throws JSONException{
        if(tags == null){
            tags = LoadTags();
        }

        String input = _readFromFile(_specSetting.getString("CardFile"));

        try {
            if (input == null || input.equals("")) {
                return null;
            }
        }
        catch (Exception e){
            return null;
        }

        JSONArray singleParse = new JSONArray(input);
        //toParse = singleParse.getJSONArray("locations");
        List<Card> returnValue = new ArrayList<Card>();
        for (int i = 0; i < singleParse.length(); i++) {
            String toAdd = singleParse.getString(i);
            Card potCard = new Card(toAdd, tags); //cannot be converted to JSON Object'
            if(potCard != null){
                returnValue.add(potCard);
            }
        }
        Card[] toTest = returnValue.toArray(new Card[returnValue.size()]);
        return toTest;
    }

    public String[] LoadTags() throws JSONException{
        if(_specSetting == null){
            return null;
        }
        else if(tags != null){
            return tags;
        }
        String input;

        input = _readFromFile(_specSetting.getString("TagFile"));

        try {
            if (input == null || input.equals("")) {
                return null;
            }
        }
        catch (Exception e){
            return null;
        }


        JSONObject singleParse = new JSONObject(input);
        //toParse = singleParse.getJSONArray("locations");
        String[] returnValue = new String[singleParse.getInt("amountTags")];
        for (int i = 0; i < returnValue.length; i++) {
            returnValue[i] = singleParse.getString(""+i);
        }
        return returnValue;
    }

    private JSONObject _loadGenSettings() throws JSONException{
        String readSettings = _readFromFile(_fileSettings);
        JSONObject returnValue;

        //If no settings are found, create a new one with default settings
        if(readSettings == null) {
            returnValue = _restartSettings();
        }
        else {
            returnValue = new JSONObject(readSettings);
            //_saveBaseSettings(returnValue);
        }
        return returnValue;
    }

    public void createSpecSettings(String name){
        //TODO sanitize the name
        //First prepare the string array to denote it in the settings file. Dont add until the end
        String[] allSpecs;

        //Now to set some default settings in the file
        JSONObject newSetting = new JSONObject();
        try{
            newSetting.put("TagFile", name+"Tags");
            newSetting.put("CardFile", name+"Cards");
            newSetting.put("DeckSize", 10); //Default
            newSetting.put("ClusterSize", 3); //Default

            JSONArray menuElements = new JSONArray();
            JSONObject tagObject = new settingListItem(
                    "TagFile", true, name+"Tags", PotTypeChange.STRING).toJSON();
            JSONObject cardObject = new settingListItem(
                    "CardFile", true, name+"Cards", PotTypeChange.STRING).toJSON();
            JSONObject deckObject = new settingListItem(
                    "DeckSize", true, "10", PotTypeChange.INT).toJSON();
            JSONObject clusterObject = new settingListItem(
                    "ClusterSize", true, "3", PotTypeChange.INT).toJSON();
            menuElements.put(tagObject);
            menuElements.put(cardObject);
            menuElements.put(deckObject);
            menuElements.put(clusterObject);
            newSetting.put("elements", menuElements);
        }
        catch (JSONException e){
            return;
            //TODO same as above
        }
        //Now to actually create the file, should just delete any existing ones
        if(saveSettings(newSetting, name)){
            //here it should change base settings to add the new setting
            String[] oldSpecs = getPosSpecSettings();
            JSONArray newSpecs = new JSONArray();
            try {
                for (int i = 0; i < oldSpecs.length; ++i) {
                    newSpecs.put(oldSpecs[i]);
                }
                newSpecs.put (name);
                _baseSettings.remove("SpecSets");
                _baseSettings.put("SpecSets", newSpecs);
                saveSettings(_baseSettings, "DomSet");

            }
            catch (JSONException e){
                //TODO figure out what to do here, restart settings maybe?
            }
        }


    }

    //TODO make this just return the currently open spec and instead use changeSpecSettings to get it
    public JSONObject loadSpecSettings(String toLoad){
        String[] actualSpec;
        try {
            actualSpec = getPosSpecSettings();
            if(actualSpec == null){
                return null;
            }
            for (String toCheck : actualSpec) {
                if (toCheck.equals(toLoad)) {
                    return new JSONObject(_readFromFile(toLoad));
                }
            }
        }
        catch (JSONException e){
            //No need to throw anything here just keep null
        }
        return null;
    }

    //TODO this is the one to use
    public JSONObject getCurrentSpecSettings(){
        return _specSetting;
    }

    public void changeSpecSettings(String toLoad){
        _specSetting = loadSpecSettings(toLoad);
        _currentOpenSetting = toLoad;
    }

    public String[] getPosSpecSettings(){
        try {
            JSONArray loadedSpec = _baseSettings.getJSONArray("SpecSets");
            String[] actualSpec = new String[loadedSpec.length()];
            for (int i = 0; i < loadedSpec.length(); ++i) {
                actualSpec[i] = loadedSpec.getString(i);
            }
            return actualSpec;
        }
        catch (JSONException e){
            return null;
        }
    }

    public JSONObject getBaseSettings(){
        return _baseSettings;
    }

    public boolean saveSettings(JSONObject toSave, String fileName){
        if(toSave == null){
            return false;
        }
        return _writeToFile(toSave.toString(), fileName);
    }

    private String _readFromFile(String theFilename) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput(theFilename);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            return null;
        } catch (IOException e) {
            return null;
        }

        return ret;
    }

    public boolean saveSingleTag(String toSave){
        if(tags == null){
            try {
                tags = LoadTags();
                if(tags == null){
                    String[] allSave = new String[1];
                    allSave[0] = toSave;
                    return saveTags(allSave);
                }
            }
            catch (JSONException e){ //try to recreate the file
                String[] allSave = new String[1];
                allSave[0] = toSave;
                return saveTags(allSave);
            }
        }
        String[] allSave = new String[tags.length+1];
        for(int i = 0; i < tags.length; ++i){
            allSave[i] = tags[i];
        }
        allSave[tags.length] = toSave;
        return saveTags(allSave);
    }

    public boolean saveTags(String[] tags){
        //JSONObject[] toSave = new JSONObject[toSaveIn.length];
        if(tags == null){
            return false;
        }
        JSONObject toSave = new JSONObject( );
        try {
            toSave.put("amountTags", tags.length);
            for (int i = 0; i < tags.length; ++i) {
                toSave.put(Integer.toString(i), tags[i]);
            }
        }
        catch (JSONException e){
            return false;
        }
        String toWrite = toSave.toString();

        try {
            try{
                String toGet = _specSetting.getString("TagFile");
            }
            catch(JSONException e){ //Assume the spec setting is corrupt and remove it
                String[] _allSpecs = getPosSpecSettings();
                int toRemove = -1;
                for(int i = 0; i < _allSpecs.length; ++i){
                    if(_allSpecs[i].equals(_currentOpenSetting)){
                        toRemove = i;
                        break;
                    }
                }
                deleteFile(_currentOpenSetting);
            }
            boolean toTest = _writeToFile(toWrite, _specSetting.getString("TagFile"));
            return toTest;
        }
        catch (JSONException e){
            return false;
        }
    }

    public boolean saveSingleCard(Card toSave){
        Card[] cards = null;
        try {
            cards = LoadCards();
        }
        catch (JSONException e){
            return false;
        }

        if(cards == null){
            cards = new Card[1];
            cards[0] = toSave;
            return saveCards(cards);
        }
        Card[] allSave = new Card[cards.length+1];
        for(int i = 0; i < cards.length; ++i){
            allSave[i] = cards[i];
        }
        allSave[cards.length] = toSave;
        boolean toTest = saveCards(allSave);
        return toTest;
    }

    public boolean saveCards(Card[] toSaveCard){
        if(toSaveCard == null){
            return false;
        }
        //JSONObject[] toSave = new JSONObject[toSaveIn.length];
        JSONArray toSave = new JSONArray();
        for (int i = 0; i < toSaveCard.length; ++i) {
            if(toSaveCard[i] != null) {
                toSave.put(toSaveCard[i].toJSON().toString());
            }
        }

        String toWrite = toSave.toString();

        try {
            boolean toTest = _writeToFile(toWrite, _specSetting.getString("CardFile"));
            return toTest;
        }
        catch (JSONException e){
            return false;
        }

    }

    private boolean _writeToFile(String data, String theFileName) {
        try {
            //TODO illegal argument here contains a path seperator
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(theFileName, Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
            return true;
        }
        catch (IOException e) {
            return false;
        }
    }

    private void _deleteSetting(String theFilename){
        deleteFile(theFilename);
        String[] posSpecs = getPosSpecSettings();
        int banned = -1;
        for(int i = 0; i < posSpecs.length; ++i){
            if(posSpecs[i].equals(theFilename)){
                banned = i;
            }
        }
        if(banned == -1){//Assume they are all corrupted
            _baseSettings = _restartSettings();
        }
        else {//just the one is corrupt
            JSONArray newSpecs = new JSONArray();
            for( int i = 0; i < posSpecs.length; ++i){
                if(i != banned){
                    newSpecs.put(posSpecs[i]);
                }
            }
            try{
                _baseSettings.remove("SpecSets");
                _baseSettings.put("SpecSets", newSpecs);
            }
            catch (JSONException e){
                _restartSettings();
            }
        }
    }

    private JSONObject _restartSettings(){
        try {
            JSONObject returnValue = new JSONObject();
            //TODO In the future there might be settings for this later, best to keep it
            JSONArray specSets = new JSONArray();
            specSets.put("Dominion");

            returnValue.put("SpecSets", specSets);

            //JSONArray menuElements = new JSONArray();
            //returnValue.put("elements", menuElements);
            saveSettings(returnValue, "DomSet");
            return returnValue;
        }
        catch(JSONException e){
            return null;
        }
    }

    public void deleteFile(String theFilename){
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(theFilename, Context.MODE_PRIVATE));
            try {
                outputStreamWriter.write("");
                outputStreamWriter.close();
            }
            catch (IOException e){
                return;
            }
        }
        catch (FileNotFoundException e){
            File newFile = new File(theFilename);
            try {
                newFile.createNewFile();
            }
            catch (IOException f){
                return;
            }
        }
    }

}
