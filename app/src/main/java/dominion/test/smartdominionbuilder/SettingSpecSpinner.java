package dominion.test.smartdominionbuilder;


import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;

import dominion.test.smartdominionbuilder.EnterNewCard.CreateCardActivity;
import dominion.test.smartdominionbuilder.EnterNewTag.CreateTagActivity;
import dominion.test.smartdominionbuilder.SettingsMenu.CustomSettings;

public class SettingSpecSpinner extends Activity implements AdapterView.OnItemSelectedListener {

    public Object callbacks;

//TODO this is never called

    public SettingSpecSpinner(Object inCallback){
        callbacks = inCallback;
    }

    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id){
        boolean toTest = false;
        //Java won't let me use a switch for this
        if(callbacks.getClass() == CustomSettings.class){
            ((CustomSettings)callbacks).setNewSpecList(pos);
        }
        else if(callbacks.getClass() == MainMenu.class){
            ((MainMenu)callbacks).changeSpecSetting(pos);
        }
        else if(callbacks.getClass() == CreateTagActivity.class){
            ((CreateTagActivity)callbacks).changeOpenSpec(pos);
        }
        else if(callbacks.getClass() == CreateCardActivity.class){
            ((CreateCardActivity)callbacks).changeSpecSetting(pos);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        //From what i understand, this is just when it loses focus.
        //if this causes it to lose text, this is prolly the method to change
    }
}
