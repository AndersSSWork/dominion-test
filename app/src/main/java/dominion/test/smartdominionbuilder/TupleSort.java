package dominion.test.smartdominionbuilder;

/**
 * Created by Anders on 03-04-2018.
 */

public interface TupleSort<T1, T2> {
    public float differenceTuple(Tuple<T1, T2> a, Tuple<T1, T2> b);
}
