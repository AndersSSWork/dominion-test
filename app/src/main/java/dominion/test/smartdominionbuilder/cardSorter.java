package dominion.test.smartdominionbuilder;

/**
 * Created by Anders on 03-04-2018.
 */

public class cardSorter<T1, T2> implements TupleSort {
    @Override
    public float differenceTuple(Tuple a, Tuple b) {
        if( a != null && b != null && (a.second instanceof Float)){
            return  ((Float)a.second - (Float)b.second);
        }
        return 0;
    }
}
