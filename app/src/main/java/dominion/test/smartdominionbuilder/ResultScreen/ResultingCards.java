package dominion.test.smartdominionbuilder.ResultScreen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;

import dominion.test.smartdominionbuilder.Card;
import dominion.test.smartdominionbuilder.CardChooseMenu.cardMenuItem;
import dominion.test.smartdominionbuilder.CardChooseMenu.selectCardsViewAdapter;
import dominion.test.smartdominionbuilder.R;

public class ResultingCards extends AppCompatActivity {

    private Card[] _cards;
    private String[] _tags;
    private cardMenuItem[] _cardItems;
    private selectCardsViewAdapter _tagAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_select_tags);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.tag_recycler);

        _cardItems = new cardMenuItem[0];
        _tagAdapter = new selectCardsViewAdapter(_cardItems);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(_tagAdapter);

        CreateMenu();
    }

    public void CreateMenu(){
        Intent intent = getIntent();
        String[] cardStrings = intent.getStringArrayExtra("_cards");
        _tags = intent.getStringArrayExtra("_tags");
        _cards = new Card[cardStrings.length-1];
        _cardItems = new cardMenuItem[_cards.length-1];
        for(int i = 0; i < cardStrings.length; ++i){
            _cards[i] = new Card(cardStrings[i], _tags);
            _cardItems[i] = new cardMenuItem(_cards[i]);
        }
        _tagAdapter.notifyDataSetChanged();
    }

    public void FinishSelecting(){
        List<String> chosenCards = new ArrayList<String>();
        List<String> chosenBan = new ArrayList<>();
        for(int i = 0; i < _tags.length; ++i){
            cardMenuItem toCheck = _tagAdapter.getItem(i);
            if(toCheck.isMust.isChecked()){
                chosenCards.add(toCheck.theCard.toString());
            }
            else if(toCheck.isBan.isChecked()){
                chosenBan.add(toCheck.theCard.toString());
            }
        }
        //open the next activity

    }
}
