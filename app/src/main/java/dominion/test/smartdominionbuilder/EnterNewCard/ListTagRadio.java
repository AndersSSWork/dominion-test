package dominion.test.smartdominionbuilder.EnterNewCard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import dominion.test.smartdominionbuilder.R;

/**
 * Created by Anders on 09-05-2018.
 */

public class ListTagRadio extends BaseAdapter implements ListAdapter {
    private ArrayList<String> list = new ArrayList<String>();
    private Context context;
    private ArrayList<String> checkList = new ArrayList<String>();


    public ListTagRadio(ArrayList<String> usedTags, ArrayList<String> output, Context context) {
        this.list = usedTags;
        this.context = context;
        this.checkList = output;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        //return list.get(pos).getId();
        //just return 0 if your list items do not have an Id variable.
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listview_tag_radio, null);
        }

        //Handle TextView and display string from your list
        TextView listItemText = (TextView)view.findViewById(R.id.TagText);
        listItemText.setText(list.get(position));

        //Handle buttons and add onClickListeners
        CheckBox checkBtn = (CheckBox) view.findViewById(R.id.tagBan);

        checkBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //do something
                boolean needAdd = true;
                for(int i = 0; i < checkList.size(); ++i){
                    if(checkList.get(i).equals(list.get(position))){
                        needAdd = false;
                        checkList.remove(i);
                        break;
                    }
                }
                if(needAdd){
                    checkList.add(list.get(position));
                }
            }
        });
        return view;
    }

    @Override
    public String toString(){
        return "it calls this";
    }
}