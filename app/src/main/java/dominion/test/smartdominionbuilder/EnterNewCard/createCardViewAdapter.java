package dominion.test.smartdominionbuilder.EnterNewCard;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import dominion.test.smartdominionbuilder.R;
import dominion.test.smartdominionbuilder.TagChooseMenu.tagMenuItem;

/**
 * Created by Anders on 01-05-2018.
 */

public class createCardViewAdapter  extends RecyclerView.Adapter<createCardViewAdapter.newCardViewHolder> {
    private newCardMenuItem[] _singleItem;

    public class newCardViewHolder extends RecyclerView.ViewHolder{
        public TextView description;
        public EditText input;
        public Button theBut;

        public newCardViewHolder(View view){
            super(view);
            description = (TextView) view.findViewById(R.id.Description);
            input = (EditText) view.findViewById(R.id.TypedIn);
            theBut = (Button) view.findViewById(R.id.DoThing);
            this.setIsRecyclable(false);
        }

        public void setClickListener(){
            theBut.setOnClickListener(new View.OnClickListener(){
                public void onClick(View view){
                    Clicked();
                }
            });
        }

        public void Clicked(){

        }

        @Override
        public String toString(){
            return description.toString();
        }

    }


    public createCardViewAdapter(newCardMenuItem[] allTags){
        _singleItem = allTags;
    }

    @Override
    public newCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tag_view_layout, parent, false);
        return new newCardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(newCardViewHolder holder, int pos){
        if(pos < _singleItem.length){
            holder.description.setText(_singleItem[pos].tagName);
            if(_singleItem[pos].whenClick != null) {
                holder.theBut.setOnClickListener(_singleItem[pos].whenClick);
            }
        }
    }

    @Override
    public int getItemCount(){
        if(_singleItem == null){
            return 0;
        }
        return _singleItem.length;
    }

    public newCardMenuItem getItem(int pos){
        if(_singleItem.length >= pos){
            return null;
        }
        else {
            return _singleItem[pos];
        }
    }
}
