package dominion.test.smartdominionbuilder.EnterNewCard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;

import java.util.ArrayList;

import dominion.test.smartdominionbuilder.Card;
import dominion.test.smartdominionbuilder.FileLoader;
import dominion.test.smartdominionbuilder.MainMenu;
import dominion.test.smartdominionbuilder.R;
import dominion.test.smartdominionbuilder.SettingSpecSpinner;

public class CreateCardActivity extends AppCompatActivity {

    private String[] _tags;

    public ListView addedTags;

    public ListTagRadio adapter;

    public ArrayList<String> allTagsDisplay;
    public ArrayList<String> usedTags;

    private boolean _isFail = false;

    private int _openSetting = 0;
    private String[] _possibleSpecs;
    public FileLoader loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_create_card);

        Intent intent = getIntent();
        loader = new FileLoader(getApplicationContext());
        try {
            _tags = loader.LoadTags();
        }
        catch (JSONException e){
            _tags = null;
        }
        allTagsDisplay = new ArrayList<>();
        if(_tags != null) {
            for (String toAdd : _tags) {
                allTagsDisplay.add(toAdd);
            }
        }
        else {
            _isFail = true;
        }
        usedTags = new ArrayList<>();
        addedTags = findViewById(R.id.usedTags);
        adapter = new ListTagRadio(allTagsDisplay, usedTags, this);
        addedTags.setAdapter(adapter);


        _possibleSpecs = loader.getPosSpecSettings();
        Spinner specSpinner = findViewById(R.id.spinner_create_card);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_dropdown_item, _possibleSpecs);
        specSpinner.setAdapter(spinnerAdapter);
        specSpinner.setOnItemSelectedListener(new SettingSpecSpinner(this));
    }

    public void AcceptNewCard(View view){
        if(_isFail){
            Cancel(view);
        }
        else {
            EditText cardText = findViewById(R.id.nameCard);
            if(usedTags.toArray() != null && usedTags.toArray().length > 0 && cardText != null) {
                String CardName = cardText.getText().toString();
                    String[] usedTagsArray = usedTags.toArray(new String[0]);;
                    Card newCard = new Card(CardName, _tags, usedTagsArray);

                    if (newCard == null) {
                        Cancel(view);
                    } else {
                        Intent intent = new Intent(this, MainMenu.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        //intent.putExtra("newCard", newCard.toString());
                        loader.saveSingleCard(newCard);
                        startActivity(intent);
                    }

            }
            else {
                Cancel(view);
            }
        }
    }

    public void Cancel(View view){
        Intent intent = new Intent(this, MainMenu.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void changeSpecSetting(int newSpec){
        _openSetting = newSpec;
        loader.changeSpecSettings(_possibleSpecs[_openSetting]);
        try {
            _tags = loader.LoadTags();
            allTagsDisplay.clear();
            if(_tags != null) {
                for (String toAdd : _tags) {
                    allTagsDisplay.add(toAdd);
                }
            }
        }
        catch (JSONException e){
            _tags = null;
        }
        usedTags.clear();

        adapter.notifyDataSetChanged();
    }
}
