package dominion.test.smartdominionbuilder.FinishMenu;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import org.w3c.dom.Text;

import dominion.test.smartdominionbuilder.CardChooseMenu.cardMenuItem;
import dominion.test.smartdominionbuilder.CardChooseMenu.selectCardsViewAdapter;
import dominion.test.smartdominionbuilder.R;

/**
 * Created by Anders on 26-04-2018.
 */

public class finishCardsViewAdapter extends RecyclerView.Adapter<finishCardsViewAdapter.finishCardView> {

    private String[] cards;

    public class finishCardView extends RecyclerView.ViewHolder{
        public TextView cardText;

        public finishCardView(View view){
            super(view);
            cardText = (TextView) view.findViewById(R.id.cardName);
            this.setIsRecyclable(false);
        }
    }

    public finishCardsViewAdapter(String[] allCards){
        cards = allCards;
    }

    @Override
    public finishCardsViewAdapter.finishCardView onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.finish_card_layout, parent, false);
        return new finishCardsViewAdapter.finishCardView(itemView);
    }

    @Override
    public void onBindViewHolder(finishCardView theView, int pos){
        if(pos < cards.length){
            theView.cardText.setText(cards[pos]);
        }
    }

    @Override
    public int getItemCount(){
        if(cards == null){
            return 0;
        }
        return cards.length;
    }

    public String getItem(int pos){
        if(cards.length >= pos){
            return null;
        }
        else {
            return cards[pos];
        }
    }
}
