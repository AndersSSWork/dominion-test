package dominion.test.smartdominionbuilder.FinishMenu;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.Random;

import dominion.test.smartdominionbuilder.Card;
import dominion.test.smartdominionbuilder.CardChooseMenu.cardMenuItem;
import dominion.test.smartdominionbuilder.CardChooseMenu.selectCardsViewAdapter;
import dominion.test.smartdominionbuilder.LearningMenu.LearningSingle.LearnSingleActivity;
import dominion.test.smartdominionbuilder.LearningMenu.LearningTwo.LearnTwoActivity;
import dominion.test.smartdominionbuilder.MainMenu;
import dominion.test.smartdominionbuilder.R;

public class FinishActivity extends AppCompatActivity {

    private Card[] _cards;
    private String[] _cardNames;
    private finishCardsViewAdapter _adapter;
    private String[] _tags;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_finish);

        CreateMenu();
        _adapter = new finishCardsViewAdapter(_cardNames);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(_adapter);
    }

    //Handles adding data to the menu and such
    public void CreateMenu(){
        Intent intent = getIntent();
        String[] _cardsString = intent.getStringArrayExtra("cards");
        _cardNames = new String[_cardsString.length];
        _tags = intent.getStringArrayExtra("_tags");
        _cards = new Card[_cardsString.length];
        for(int i = 0; i < _cardsString.length; ++i){
            _cards[i] = new Card(_cardsString[i], _tags);
            _cardNames[i] = _cards[i].name;
        }
    }

    public void GotoLearn(View view){
        Intent intent = getIntent();
        _tags = intent.getStringArrayExtra("_tags");
        String[] cardStrings = intent.getStringArrayExtra("cards");

        Random rand = new Random();
        intent = rand.nextFloat() > 0.5 ?
                new Intent(this, LearnSingleActivity.class) :
                new Intent(this, LearnTwoActivity.class);

        intent.putExtra("cards", cardStrings);
        intent.putExtra("tags", _tags);
        startActivity(intent);
    }

}
